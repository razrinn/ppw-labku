from django.urls import path
from . import views
# url for app
app_name = 'story_3'

urlpatterns = [
    path('', views.index, name='index'),
    path('about/', views.about, name='about'),
    path('education/', views.education, name='education'),
    path('skill/', views.skill, name='skill'),
    path('experience/', views.experience, name='experience'),
    path('contact/', views.contact, name='contact'),
    path('register/', views.register, name='register'),
    path('schedule/add/', views.schedule_creation, name='schedule_creation'),
    path('schedule/', views.schedule_list, name='schedule_list'),
    path('delete/', views.schedule_drop_all, name='schedule_drop_all'),
]
