from django.shortcuts import render, redirect
from .models import Schedule
from . import forms

# Create your views here.


def index(request):
    return render(request, 'base_home.html')


def about(request):
    return render(request, 'base_about.html')


def education(request):
    return render(request, 'base_education.html')


def skill(request):
    return render(request, 'base_skill.html')


def experience(request):
    return render(request, 'base_experience.html')


def contact(request):
    return render(request, 'base_contact.html')


def register(request):
    return render(request, 'base_register.html')


def schedule_list(request):
    schedules = Schedule.objects.all().order_by('date')
    return render(request, 'base_schedule_list.html', {'schedules': schedules})


def schedule_creation(request):
    if request.method == 'POST':
        form = forms.ScheduleForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('story_3:schedule_list')

    else:
        form = forms.ScheduleForm()
    return render(request, 'base_schedule_creation.html', {'form': form})


def schedule_drop_all(request):
    Schedule.objects.all().delete()
    return redirect('story_3:schedule_list')
